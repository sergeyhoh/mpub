set_default(:mysql_admin) { Capistrano::CLI.ui.ask "Username with priviledged database access (to create db):" }
set_default(:mysql_host, "localhost")
set_default(:mysql_user) { user }
set_default(:mysql_password) { Capistrano::CLI.password_prompt "Enter #{rails_env} database password:: " }
set_default(:mysql_database) { "#{application}_#{rails_env}" }

namespace :db do
  namespace :mysql do
    desc "Install packages to build mysql2 gem"
    task :install, roles: :db, only: { primary: true } do
      run "#{sudo} sudo apt-get -y install libmysqlclient-dev"
    end
    after "deploy:install", "db:mysql:install"

    desc "Create a database for this application."
    task :create_database, roles: :db, only: { primary: true } do
      #prepare_for_db_command

      sql = <<-SQL
        CREATE DATABASE #{mysql_database};
        GRANT ALL PRIVILEGES ON #{mysql_database}.* TO #{mysql_user}@localhost IDENTIFIED BY '#{mysql_password}';
      SQL

      run "mysql --user=#{mysql_admin} -p --execute=\"#{sql}\"" do |channel, stream, data|
        p data
        if data =~ /^Enter password:/
          pass = Capistrano::CLI.password_prompt "Enter database password for '#{mysql_admin}':"
          channel.send_data "#{pass}\n"
        end
      end
    end
    after "deploy:setup", "db:mysql:create_database"

    desc "Generate the database.yml configuration file."
    task :setup, roles: :app do
      run "mkdir -p #{shared_path}/config"
      template "mysql.yml.erb", "#{shared_path}/config/database.yml"
    end
    after "deploy:setup", "db:mysql:setup"

    desc "Symlink the database.yml file into latest release"
    task :symlink, roles: :app do
      run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end
    after "deploy:finalize_update", "db:mysql:symlink"
  end

  desc "Populates the database with seed data"
  task :seed do
    Capistrano::CLI.ui.say "Populating the database..."
    run "cd #{current_path}; bundle exec rake RAILS_ENV=#{rails_env} db:seed"
  end
end