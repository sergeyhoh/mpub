namespace :uploads do
  desc "Generate uploads folder."
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/uploads"
  end
  after "deploy:setup", "uploads:setup"

  desc "Symlink to the uploads folder on the latest release"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/uploads #{release_path}/uploads"
  end
  after "deploy:finalize_update", "uploads:symlink"
end
