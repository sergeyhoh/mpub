set_default(:unicorn_user) { user }

set_default(:unicorn_config)    { "#{shared_path}/config/unicorn.rb" }
set_default(:unicorn_pid_dir)   { "#{shared_path}/pids" }
set_default(:unicorn_log_dir)   { "#{shared_path}/log" }
set_default(:unicorn_workers, 2)

namespace :unicorn do
  desc "Setup Unicorn initializer and app configuration"
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "unicorn.rb.erb", unicorn_config
    template "unicorn_init.sh.erb", "#{shared_path}/unicorn_init"
    run "chmod +x #{shared_path}/unicorn_init"
    run "#{sudo} mv #{shared_path}/unicorn_init /etc/init.d/unicorn_#{application}"
    run "#{sudo} update-rc.d -f unicorn_#{application} defaults"
  end
  after "deploy:setup", "unicorn:setup"

  %w[start stop restart upgrade].each do |command|
    desc "#{command.capitalize} unicorn server"
    task command, roles: :app do
      run "/etc/init.d/unicorn_#{application} #{command}"
    end
    after "deploy:#{command}", "unicorn:#{command}"
  end
end
