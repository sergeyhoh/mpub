namespace :nginx do
  desc "Install nginx from stable repository"
  task :install, roles: :web do
    run "#{sudo} apt-get -y install nginx"
    run "#{sudo} apt-get -y install apache2-utils"
  end
  after "deploy:install", "nginx:install"

  desc "Setup nginx ssl configuration"
  task :setup_ssl, roles: :web do
    cert_password = Capistrano::CLI.password_prompt "Enter password for new certificate:: "

    run "#{sudo} openssl genrsa -des3 -out /etc/ssl/#{application}.key 1024" do |channel, stream, data|
      if data =~ /^Enter pass phrase for/
        channel.send_data "#{cert_password}\n"
      elsif data =~ /^Verifying - Enter pass phrase for/
        channel.send_data "#{cert_password}\n"
      end
    end
    run "#{sudo} openssl req -new -key /etc/ssl/#{application}.key -out /etc/ssl/#{application}.csr" do |channel, stream, data|
      if data =~ /^Enter pass phrase for/
        channel.send_data "#{cert_password}\n"
      elsif data =~ /^Country Name/
        channel.send_data "US\n"
      elsif data =~ /^State or Province Name/
        channel.send_data "New York\n"
      elsif data =~ /^Locality Name/
        channel.send_data "New York\n"
      elsif data =~ /^Organization Name/
        channel.send_data "EMNA\n"
      elsif data =~ /^Common Name/
        channel.send_data "\n"
      elsif data =~ /^Email Address/
        channel.send_data "\n"
      elsif data =~ /\[\]\:$/
        channel.send_data "\n"
      end
    end
    run "#{sudo} cp /etc/ssl/#{application}.key /etc/ssl/#{application}.key.org"
    run "#{sudo} openssl rsa -in /etc/ssl/#{application}.key.org -out /etc/ssl/#{application}.key" do |channel, stream, data|
      if data =~ /^Enter pass phrase for/i
        channel.send_data "#{cert_password}\n"
      end
    end
    run "#{sudo} openssl x509 -req -days 365 -in /etc/ssl/#{application}.csr -signkey /etc/ssl/#{application}.key -out /etc/ssl/#{application}.crt"
    run "#{sudo} mv /etc/ssl/#{application}.crt /etc/ssl/certs/#{application}.crt"
    run "#{sudo} mv /etc/ssl/#{application}.key /etc/ssl/private/#{application}.key"
    run "#{sudo} chmod 640 /etc/ssl/private/#{application}.key"
    run "#{sudo} chown root:root /etc/ssl/private/#{application}.key"
    run "#{sudo} rm -f /etc/ssl/#{application}.csr /etc/ssl/#{application}.key.orig"

    #template "nginx/redirect_to_ssl.erb", "#{shared_path}/all_to_ssl"
    #run "#{sudo} mv #{shared_path}/all_to_ssl /etc/nginx/sites-enabled/all_to_ssl"
  end
  after "deploy:setup", "nginx:setup_ssl"

  desc "Setup nginx basic auth password"
  task :setup_passwd, roles: :web do
    nginx_password = Capistrano::CLI.password_prompt "Enter password for Nginx HTTP Basic Authentication:: "

    run "#{sudo} htpasswd -cd /etc/nginx/auth.passwd admin" do |channel, stream, data|
      if data =~ /^New password:/i
        channel.send_data "#{nginx_password}\n"
      elsif data =~ /Re-type new password:/i
        channel.send_data "#{nginx_password}\n"
      end
    end
    run "#{sudo} chown root:root /etc/nginx/auth.passwd"
    run "#{sudo} chmod 644 /etc/nginx/auth.passwd"
  end
  after "deploy:setup", "nginx:setup_passwd"

  desc "Setup nginx configuration for this application"
  task :setup, roles: :web do
    template "nginx/proxy.conf.erb", "#{shared_path}/proxy.conf"
    run "#{sudo} mv #{shared_path}/proxy.conf /etc/nginx/proxy.conf"

    template "nginx/unicorn.conf.erb", "#{shared_path}/#{application}"
    run "#{sudo} mv #{shared_path}/#{application} /etc/nginx/sites-enabled/#{application}"
    run "#{sudo} rm -f /etc/nginx/sites-enabled/default"
    restart
  end
  after "deploy:setup", "nginx:setup"

  %w[start stop restart].each do |command|
    desc "#{command.capitalize} nginx"
    task command, roles: :web do
      run "#{sudo} service nginx #{command}"
    end
  end
end
