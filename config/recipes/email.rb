set_default(:email_username) { Capistrano::CLI.ui.ask "Email username:" }
set_default(:email_password) { Capistrano::CLI.ui.ask "Email password:" }

namespace :email do
  desc "Setup email parameters"
  task :setup, roles: :app do
    run "echo export GMAIL_USERNAME=\"#{email_username}\" >> ~/.bashrc"
    run "echo export GMAIL_PASSWORD=\"#{email_password}\" >> ~/.bashrc"
  end
  after "deploy:setup", "email:setup"
end
