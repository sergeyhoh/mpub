CarrierWave.configure do |config|
  config.root = Rails.root
  config.cache_dir = 'tmp/carrierwave'
end
