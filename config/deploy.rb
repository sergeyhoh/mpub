require 'rvm/capistrano' # Для работы rvm
require 'bundler/capistrano' # Для работы bundler. При изменении гемов bundler автоматически обновит все гемы на сервере, чтобы они в точности соответствовали гемам разработчика.

load "config/recipes/base"
load "config/recipes/email"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/mysql"
load "config/recipes/uploads"
load "config/recipes/check"

# Fix issue when development machine is not in ~/.ssh/known_hosts of deploy server
ssh_options[:forward_agent] = true
default_run_options[:pty] = true

set :application, "mpub"
set :user, "mpub"
set :rails_env, "production"
set :domain, "mpub@86.57.160.41" # Это необходимо для деплоя через ssh. Именно ради этого я настоятельно советовал сразу же залить на сервер свой ключ, чтобы не вводить паролей.
set :deploy_to, "/srv/#{application}"
set :use_sudo, false
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"

set :rvm_ruby_string, '1.9.3' # Это указание на то, какой Ruby интерпретатор мы будем использовать.

set :port, 22
set :scm, :git # Используем git. Можно, конечно, использовать что-нибудь другое - svn, например, но общая рекомендация для всех кто не использует git - используйте git.
set :repository,  "ssh://git@bitbucket.org/sergeyhoh/mpub.git" # Путь до вашего репозитария. Кстати, забор кода с него происходит уже не от вас, а от сервера, поэтому стоит создать пару rsa ключей на сервере и добавить их в deployment keys в настройках репозитария.
set :branch, "master" # Ветка из которой будем тянуть код для деплоя.
set :deploy_via, :remote_cache # Указание на то, что стоит хранить кеш репозитария локально и с каждым деплоем лишь подтягивать произведенные изменения. Очень актуально для больших и тяжелых репозитариев.

role :web, domain
role :app, domain
role :db,  domain, :primary => true

# Create project dir under deploy user
before "deploy:setup" do
  run "#{sudo} mkdir -p /srv/#{application}"
  run "#{sudo} chown #{user}:#{user} /srv/#{application}"
end

after "deploy", "deploy:cleanup" # keep only the last 5 releases

after 'deploy:update_code', :roles => :app do
  # Установка необходимых gem файлов для проекта
  run "cd #{latest_release} && bundle install --deployment --without development test"
end

###########################################################################################################################


namespace :gems do
  desc "Install gems"
  task :install, :roles => :app do
    # Установка необходимых gem файлов для проекта
    run "cd #{latest_release} && bundle install --deployment --without development test"
  end
end
