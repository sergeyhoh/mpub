Mpub::Application.routes.draw do

  get "home/index"

  devise_for :users, :skip => [:registrations]
  devise_scope :user do
    get    '/login'  => 'devise/sessions#new'
    post   '/login'  => "devise/sessions#create"
    delete '/logout' => 'devise/sessions#destroy'
  end
  resources :users do
    get "info", :on => :member
    get "updated", :on => :member
  end

  namespace :api do
    #devise_for :users, :skip => [:sessions, :registrations, :confirmations, :passwords, :unlocks]
    devise_scope :user do
      post "/login" => "sessions#create", :as => :user_session
      delete "/logout" => "sessions#destroy", :as => :destroy_user_session
      get "/status" => "sessions#status"
    end

    resources :categories, :only => [:index, :show]
    match "structure" => 'categories#structure', :via => :get
    match "categories/uploads/:id/:type/:basename.:extension" => 'categories#download', :via => :get, :type => /orig|thumb/

    resources :sub_categories, :only => [:index, :show]
    match "sub_categories/uploads/:id/:type/:basename.:extension" => 'sub_categories#download', :via => :get, :type => /orig|thumb/

    resources :contents, :only => [:index, :show]
    match "contents/uploads/:id/:basename.:extension" => 'contents#download', :via => :get

    match "company" => 'companies#info', :via => :get
    match "companies/uploads/:id/:type/:basename.:extension" => 'companies#download', :via => :get, :type => /orig|thumb/

    match "user_info" => 'users#info', :via => :get
    match "user_profiles/uploads/:id/:type/:basename.:extension" => 'user_profiles#download', :via => :get, :type => /orig|thumb/
    match "customizations/uploads/:id/:type/:basename.:extension" => 'customizations#download', :via => :get, :type => /orig/

    resources :contacts, :only => [:create]

    match "updated" => 'users#updated', :via => :get
  end

  match "profile/:id" => "user_profiles#show", :via => :get, :as => :profile
  match "/:controller/uploads/:id/:type/:basename.:extension" => 'user_profiles#download', :via => :get, :type => /orig|thumb/

  resources :companies
  match "/:controller/uploads/:id/:type/:basename.:extension" => 'companies#download', :via => :get, :type => /orig|thumb/

  resources :customizations, :only => [:edit, :update, :show]
  match "/:controller/uploads/:id/:type/:basename.:extension" => 'customizations#download', :via => :get, :type => /orig|thumb/

  resources :categories do
    resources :contents do
      put 'activate', :on => :member
      put 'unlink', :on => :member
    end
    put 'activate', :on => :member
  end
  match "/:controller/uploads/:id/:type/:basename.:extension" => 'categories#download', :via => :get, :type => /orig|thumb|small/

  resources :sub_categories do
    resources :contents do
      put 'activate', :on => :member
      put 'unlink', :on => :member
    end
    put 'activate', :on => :member
  end
  match "/:controller/uploads/:id/:type/:basename.:extension" => 'sub_categories#download', :via => :get, :type => /orig|thumb/

  resources :contents do
    put 'activate', :on => :member
    put 'unlink', :on => :member
  end
  match "/:controller/uploads/:id/:basename.:extension" => 'contents#download', :via => :get

  mount Ckeditor::Engine => '/ckeditor'
  match "uploads/ckeditor/pictures/:id/:basename.:extension" => 'ckeditor/pictures#download', :via => :get
  match "uploads/ckeditor/attachments/:id/:basename.:extension" => 'ckeditor/attachment_files#download', :via => :get

  resources :contacts, :except => [:edit, :update]
  match "contact_us" => 'contacts#new', :via => :get

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  authenticated :user do
    root :to => 'companies#index', :constraints => RoleConstraint.new(:admin)
    root :to => 'categories#index'
  end

  root :to => redirect("/login")
  #root :to => "home#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
