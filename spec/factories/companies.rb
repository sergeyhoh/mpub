# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    avatar "MyString"
    name "MyString"
    phone "MyString"
    fax "MyString"
    website "MyString"
    info "MyText"
    address "MyText"
  end
end
