# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do
    company_id 1
    first_name "MyString"
    second_name "MyString"
    email "MyString"
    phone "MyString"
    message "MyString"
  end
end
