# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :customization do
    user_id 1
    background "MyString"
    text_color "MyString"
    navbar_color "MyString"
  end
end
