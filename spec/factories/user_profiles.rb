# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_profile do
    user_id 1
    avatar "MyString"
    full_name "MyString"
    phone "MyString"
    fax "MyString"
    company "MyString"
    company_info "MyText"
    address "MyText"
  end
end
