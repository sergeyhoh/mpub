# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :content do
    sub_category_id 1
    user_id 1
    name "MyString"
    file "MyString"
    file_size 1
    content_type "MyString"
  end
end
