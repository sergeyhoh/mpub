class Contention < ActiveRecord::Base
  belongs_to :contentable, :polymorphic => true
  belongs_to :content
end
