class Company < ActiveRecord::Base
  resourcify

  attr_accessible :address, :avatar, :avatar_cache, :remove_avatar, :info, :fax, :name, :phone, :website

  has_many :users, :dependent => :destroy
  has_many :contacts, :dependent => :destroy

  mount_uploader :avatar, AvatarUploader

  # validates_presence_of   :avatar
  # validates_integrity_of  :avatar
  # validates_processing_of :avatar
  validates :name, length: { maximum: 100 }, format: { with: /\A[[:word:]\s\-]+\z/ }, presence: true
  validates :website, length: { maximum: 100 }, format: { with: /^(http[s]?:\/\/){0,1}(www\.){0,1}[[:word:]\.\-]+\.[[:word:]]{2,5}[\.]{0,1}/i }, :allow_blank => true
  validates :phone, :fax, length: { maximum: 20 }, format: { with: /\A[\w\-\+\(\)\[\] \#\*]+\z/ }, :allow_blank => true
  validates :info, :address, length: { maximum: 1000 }
end
