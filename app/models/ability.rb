class Ability
  include CanCan::Ability

  def initialize(user)
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :company_admin
      if !user.company_id.to_s.blank?
        can :show, Role
        can :show, Company, id: user.company_id
        can :create, User
        can :manage, User, company_id: user.company_id
        can :manage_device_udid, User, company_id: user.company_id

        can [:manage, :download], UserProfile do |up|
          up.user.company_id == user.company_id
        end

        can :manage, Contact, company_id: user.company_id
      end

      can :create, Content
      can :manage, Content, user_id: user.id
      can :create, Category
      can :manage, Category, user_id: user.id
      can :create, SubCategory
      can :manage, SubCategory, user_id: user.id
    elsif user.has_role? :company_user
      can :create, Content
      can :manage, Content, user_id: user.id
      can :create, Category
      can :manage, Category, user_id: user.id
      can :create, SubCategory
      can :manage, SubCategory, user_id: user.id
    end
    default_rules(user)
  end

  def default_rules(user)
    can :update, [User], id: user.id
    can :info, [User], id: user.id
    can :updated, [User], id: user.id
    can :show, [UserProfile], user_id: user.id
    can :download, [UserProfile], user_id: user.id
    can :show, [Customization], user_id: user.id
    can :update, [Customization], user_id: user.id
    can :download, [Customization], user_id: user.id

    can :create, Contact

    # Always performed
    can :access, :ckeditor   # needed to access Ckeditor filebrowser

    # Performed checks for actions:
    can [:read, :create, :destroy, :download], Ckeditor::Picture
    can [:read, :create, :destroy, :download], Ckeditor::AttachmentFile
  end
end
