class Contact < ActiveRecord::Base
  resourcify

  attr_accessible :company_id, :email, :first_name, :message, :phone, :second_name, :device_udid

  belongs_to :company

  validates :company_id, presence: true
  validates :first_name, length: { maximum: 100 }, format: { with: /\A[[:word:]\s\-]+\z/ }, presence: true
  validates :second_name, length: { maximum: 100 }, format: { with: /\A[[:word:]\s\-]+\z/ }, presence: true
  validates :email, format: { with: /\A[^@\s]+@(?:[-a-z0-9]+\.)+[a-z]{2,}\z/ }, presence: true
  validates :phone, length: { maximum: 20 }, format: { with: /\A[\w\-\+\(\)\[\] \#\*]+\z/ }, :allow_blank => true
  validates :message, length: { maximum: 10000 }, presence: true
  validates :device_udid, length: { is: 40 }, allow_blank: true

  validate :validate_company_id

  private

    def validate_company_id
      if !Company.exists?(self.company_id)
        errors.add(:company_id, "is invalid")
      end
    end
end
