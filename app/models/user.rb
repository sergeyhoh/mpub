class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :registerable
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :token_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable, :lockable, :timeoutable

  belongs_to :company

  # Relations between users parameters
  has_one :user_profile, :dependent => :destroy
  accepts_nested_attributes_for :user_profile
  has_one :customization, :dependent => :destroy

  # Relations between content
  has_many :categories, :dependent => :destroy
  has_many :sub_categories, :dependent => :destroy
  has_many :contents, :dependent => :destroy

  # Create some attributes before create user
  before_create :create_login, :set_api_updated_at, :generate_user_token
  after_destroy :ensure_an_admin_remains
  before_save :create_customization, :reset_authentication_token, :ensure_user_token


  # Setup accessible (or protected) attributes for your model
  attr_accessible :company_id, :name, :email, :password, :password_confirmation,
                  :remember_me, :role_ids, :user_profile_attributes, :device_udid

  validates :name, :email, presence: true
  validates :name, :email, :uniqueness => { case_sensitive: false }
  validates :device_udid, :uniqueness => { case_sensitive: false }, length: { is: 40 }, allow_blank: true

  def create_login
    email = self.email.split(/@/)
    login_taken = User.where( :login => email[0]).first
    unless login_taken
      self.login = email[0]
    else
      self.login = self.email
    end
  end

  def set_api_updated_at
    self.api_updated_at = Time.now
  end

  def self.find_for_database_authentication(conditions)
    self.where(:login => conditions[:email]).first || self.where(:email => conditions[:email]).first
  end

  def role?(role)
    return !!self.roles.find_by_name(role.to_s)
  end

  # Fetch current user in models
  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  private

  def ensure_an_admin_remains
    if User.with_role(:admin).count.zero?
      raise "Can't delete last admin user"
    end
  end

  def create_customization
    self.customization ||= Customization.new({ :user_id => self.id })
  end

  def generate_user_token
    #SecureRandom.base64(15).tr('+/=lIO0', 'pqrsxyz')
    loop do
      token = Devise.friendly_token
      self.user_token = token
      break token unless User.where(:user_token => token).first
    end
  end

  def ensure_user_token
    generate_user_token if self.user_token.blank?
  end

end
