# encoding: utf-8

class Category < ActiveRecord::Base
  resourcify

  attr_accessible :active, :name, :user_id, :art, :art_cache, :remove_art, :description

  belongs_to :user, :touch => :api_updated_at

  has_many :sub_categories, :dependent => :destroy, :uniq => true

  has_many :contentions, :as => :contentable, :dependent => :destroy
  has_many :contents, :through => :contentions

  mount_uploader :art, CategoryArtUploader

  validates :name, presence: true,
      uniqueness: { case_sensitive: false, :scope => :user_id },
      length: { maximum: 50 },
      format: { with: /\A[[:word:]\s\-\.\!\?]+\z/i }
  validates :active, :inclusion => { in: [true, false] }
end
