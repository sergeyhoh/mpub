class Customization < ActiveRecord::Base
  resourcify

  attr_accessible :background_color, :background_image, :background_image_cache, :remove_background_image, :text_color, :user_id

  belongs_to :user, :touch => :api_updated_at

  mount_uploader :background_image, CustomizationUploader

  validates :user_id, presence: true
  validates :background_color, :text_color, presence: true
  validates :background_color, :text_color, :length => { maximum: 7 }
  validates :background_color, :text_color, :format => { with: /^#(\w{3}|\w{6})\b$/i }
end
