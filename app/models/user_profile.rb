class UserProfile < ActiveRecord::Base
  resourcify

  attr_accessible :address, :avatar, :avatar_cache, :remove_avatar, :info, :fax, :full_name, :phone, :user_id

  belongs_to :user, :touch => :api_updated_at

  mount_uploader :avatar, AvatarUploader

  # validates_presence_of   :avatar
  # validates_integrity_of  :avatar
  # validates_processing_of :avatar
  validates :full_name, length: { maximum: 100 }, format: { with: /\A[[:word:]\s\-]+\z/ }, :allow_blank => true
  validates :phone, length: { maximum: 20 }, format: { with: /\A[\w\-\+\(\)\[\] \#\*]+\z/ }, :allow_blank => true
  validates :fax, length: { maximum: 20 }, format: { with: /\A[\w\-\+\(\)\[\] \#\*]+\z/ }, :allow_blank => true
  validates :info, length: { maximum: 1000 }
end
