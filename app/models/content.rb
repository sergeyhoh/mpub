class Content < ActiveRecord::Base
  resourcify

  EXTENSIONS = ['html', 'txt']

  attr_accessor :ck_name, :ck_content, :ck_extension
  attr_accessible :ck_name, :ck_content, :ck_extension, :category_ids, :sub_category_ids
  attr_accessible :active, :file, :file_cache, :name, :remove_file, :sub_category_name, :user_id

  belongs_to :user, :touch => :api_updated_at

  has_many :contentions, :dependent => :destroy
  #has_many :contentable, :through => :contentions
  has_many :categories, :through => :contentions, :source => :contentable, :source_type => "Category"
  has_many :sub_categories, :through => :contentions, :source => :contentable, :source_type => "SubCategory"

  mount_uploader :file, ContentUploader

  # add ckeditor content to file
  after_initialize :get_ckdata # convert db format to accessors
  before_validation :set_ckdata # convert accessors back to db format
  # update some file attributes
  before_save :update_file_attributes

  validates :file, presence: true
  validates :name, presence: true, length: { maximum: 50 }
  validates :active, :inclusion => { in: [true, false] }
  # ck_extension is a index of EXTENSIONS
  validates :ck_extension, :numericality => { only_integer: true }

  validates :category_ids, presence: true, :if => lambda { self.sub_category_ids.blank? }
  validates :sub_category_ids, presence: true, :if => lambda { self.category_ids.blank? }

  def category_ids
    self.categories.map(&:id)
  end

  def category_ids=(ids)
    ids = ids.delete_if(&:empty?)
    self.categories = Category.find(ids)
  end

  def sub_category_ids
    self.sub_categories.map(&:id)
  end

  def sub_category_ids=(ids)
    ids = ids.delete_if(&:empty?)
    self.sub_categories = SubCategory.find(ids)
  end

private

  def update_file_attributes
    if file.present? && file_changed?
      self.extension = file.file.extension.downcase
      self.content_type = file.file.content_type
      self.file_size = file.file.size
      self.md5_sum = ::Digest::MD5.file(File.join(Rails.root, file.url.to_s)).hexdigest
    end
  end

  def get_ckdata
    if Content::EXTENSIONS.include?(self.extension)
      self.ck_name = File.basename(self.file.url, '.*')
      self.ck_content = ''
      self.ck_extension = Content::EXTENSIONS.index(self.extension)

      File.open(self.file.current_path, 'r') do |f|
        f.each do |line|
          self.ck_content << line
        end
      end
    end
  end

  def set_ckdata
    if !self.ck_name.blank? && !self.ck_content.blank? && !self.ck_extension.blank?
      tmp_extension = Content::EXTENSIONS[self.ck_extension.to_i].nil? ? 0 : Content::EXTENSIONS[self.ck_extension.to_i]

      iofile = AppSpecificStringIO.new("#{self.ck_name}.#{tmp_extension}", self.ck_content)
      self.file = iofile
    end
  end
end
