class SubCategory < ActiveRecord::Base
  resourcify

  attr_accessible :active, :category_id, :category_name, :name, :user_id, :art, :art_cache, :remove_art, :description

  belongs_to :user, :touch => :api_updated_at
  belongs_to :category, :touch => true

  has_many :contentions, :as => :contentable, :dependent => :destroy
  has_many :contents, :through => :contentions

  mount_uploader :art, SubCategoryArtUploader

  validates :category_id, presence: true
  validates :name, presence: true,
      uniqueness: { case_sensitive: false, :scope => [:user_id, :category_id] },
      length: { maximum: 50 },
      format: { with: /\A[[:word:]\s\-\.\!\?]+\z/i }
  validates :description, length: { maximum: 500 }, allow_blank: true
  validates :active, :inclusion => { in: [true, false] }

  def category_name
    category.try(:name)
  end

  def category_name=(name)
    self.category = Category.where(user_id: User.current.id).find_or_create_by_name(name) if name.present?
  end

  def self.for_select(current_user_id)
    Category.where(user_id: current_user_id).map do |category|
      [category.name, category.sub_categories.map { |l| [l.name, l.id] }.sort]
    end
  end
end
