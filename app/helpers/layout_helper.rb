# These helper methods can be called in your template to set variables to be used in the layout
# This module should be included in all views globally,
# to do so you may need to add this line to your ApplicationController
#   helper :layout
module LayoutHelper
  def javascript(*files)
    content_for(:head) { javascript_include_tag(*files) }
  end

  def title(page_title, show_title = true)
    content_for(:title) { h(page_title.to_s) }
    @show_title = show_title
  end

  def show_title?
    @show_title
  end

  def new_item(item_title, item_url, options = {}, show_new_item = true)
    content_for(:new_item) { content_tag(:li, link_to(item_title, item_url, options)) }
    @show_new_item = show_new_item
  end

  def show_new_item?
    @show_new_item
  end

  def body_style
    style_attr = ""
    if user_signed_in?
      style_attr << "background-image:url('" + mpub_image_url(id: current_user.customization.id, type: :orig, uploader: current_user.customization.background_image, cname: c_name(current_user.customization)) + "');" if current_user.customization.background_image?
      style_attr << "background-color:" + current_user.customization.background_color + ";" if current_user.customization.background_color
      style_attr << "color:" + current_user.customization.text_color + ";" if current_user.customization.text_color
    end

    style_attr
  end
end
