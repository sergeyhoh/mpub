module GaleryHelper
  def galery(columns, categories, &block)
    Galery.new(self, columns, categories, block).galery_table
  end

  class Galery < Struct.new(:view, :columns, :categories, :callback)
    HEADER = %w[Monday Tuesday Wednesday Thursday Friday Saturday Sunday]
    START_DAY = :monday

    delegate :content_tag, to: :view

    def galery_table
      content_tag :table, class: "galery table table-center sortable" do
        galery_header + galery_rows
      end
    end

    def galery_header
      #   content_tag :tr do
      #     HEADER.map { |day| content_tag :th, day }.join.html_safe
      #   end
      content_tag :tr do
        content_tag :th, colspan: 3 do
          content_tag :blockquote, class: 'pull-right' do
            view.sortable("name", "Sort by Name", "categories")
          end
        end
      end
    end

    def galery_rows
      galery_columns.map do |row|
        content_tag :tr do
          row.map { |id, active| galery_cell(id, active) }.join.html_safe
        end
      end.join.html_safe
    end

    def galery_columns
      # first = date.beginning_of_month.beginning_of_week(START_DAY)
      # last = date.end_of_month.end_of_week(START_DAY)
      # (first..last).to_a.in_groups_of(7)

      categories.map { |cat| [ cat.id, cat.active ] }.in_groups_of(columns)
    end

    def galery_cell(id, active)
      return '<td></td>' if id.nil?
      content_tag :td, view.capture(id, &callback), class: active ? 'centered' : 'centered deactivated'
    end

  end
end
