module MpubUrlHelper
  def mpub_image_tag(args = {})
    image_tag(mpub_image_url(args), :class => "art img-rounded")
  end

  def mpub_image_url(args = {})
    UrlGenerator.new(args[:id], args[:type], args[:uploader], args[:cname]).url_for_image
  end

  def mpub_file_url(args = {})
    UrlGenerator.new(args[:id], args[:type], args[:uploader], args[:cname]).url_for_file
  end

  def mpub_api_image_url(args = {})
    UrlGenerator.new(args[:id], args[:type], args[:uploader], args[:cname]).api_url_for_image
  end

  def mpub_api_file_url(args = {})
    UrlGenerator.new(args[:id], args[:type], args[:uploader], args[:cname]).api_url_for_file
  end

  class UrlGenerator < Struct.new(:id, :type, :uploader, :cname)
    def url_for_image
      self.type = Array[type] if !type.blank? && !type.is_a?(Array)

      if id.blank? || uploader.blank? || cname.blank? || type.blank?
        get_default_image_url
      else
        image_url
      end
    end

    def url_for_file
      if id.blank? || uploader.blank? || cname.blank?
        "defaults/empty.txt"
      else
        "/#{cname}/uploads/#{id}/#{get_file_name}"
      end
    end

    def api_url_for_image
      "/api#{url_for_image}"
    end

    def api_url_for_file
      "/api#{url_for_file}"
    end

  private

    def image_url
      "/#{cname}/uploads/#{id}/#{type.first.to_s}/#{get_file_name_by_type}"
    end

    def get_default_image_url
      if !type.blank? && type.first == :small
        small_defult_image_url
      else
        default_image_url
      end
    end

    def default_image_url
      if cname && cname == 'sub_categories'
        "/assets/defaults/default_150x100.png"
      else
        "/assets/defaults/default_200x200.png"
      end
    end

    def small_defult_image_url
      if cname && cname == 'sub_categories'
        "/assets/defaults/default_75x50.png"
      else
        "/assets/defaults/default_50x50.png"
      end
    end

    def get_file_name
      File.basename(uploader.url)
    end

    def get_file_name_by_type
      if type.first == :orig
        File.basename(uploader.url)
      else
        File.basename(uploader.url(*type))
      end
    end
  end
end
