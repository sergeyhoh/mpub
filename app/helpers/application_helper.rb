module ApplicationHelper
  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end

  def url_with_protocol(url)
    /^http/.match(url) ? url : "http://#{url}"
  end

  def c_name(some_class)
    some_class.nil? ? '' : some_class.class.name.to_s.pluralize.underscore
  end

  # set sortable head in tables
  def sortable(column, title = nil, controller=nil)
    controller ||= controller_name.underscore
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"

    link_to title, params.merge(:controller => controller, :sort => column, :direction => direction, :page => nil), {:remote => true, :class => css_class}
  end
end
