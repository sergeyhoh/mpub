module IconizeHelper
  def iconize_show
    iconize('', 'icon-info-sign')
  end

  def iconize_edit
    iconize('', 'icon-edit')
  end

  def iconize_destroy
    iconize('', 'icon-remove-sign')
  end

  def iconize_unlink
    iconize('', 'icon-minus-sign')
  end

  def iconize_is_active(active)
    if active
      iconize('','icon-eye-open')
    else
      iconize('', 'icon-eye-close')
    end
  end

  def iconize_show_text
    iconize(' Show', 'icon-info-sign')
  end

  def iconize_edit_text
    iconize(' Edit', 'icon-edit')
  end

  def iconize_destroy_text
    iconize(' Destroy', 'icon-remove-sign')
  end

  def iconize_unlink_text
    iconize(' Unlink', 'icon-minus-sign')
  end

  def iconize_is_active_text(active)
    if active
      iconize(' Deactivate', 'icon-eye-close')
    else
      iconize(' Activate','icon-eye-open')
    end
  end

  def iconize(text, icon_name)
    "<i class=\"#{icon_name}\"></i>#{text}".html_safe
  end
end