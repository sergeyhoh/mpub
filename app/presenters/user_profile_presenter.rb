class UserProfilePresenter < BasePresenter
  presents :user_profile
  #delegate :full_name, to: :user_profile

  def linked_name
    user_profile.user.name.present? ? user_profile.user.name : user_profile.full_name
  end

  def avatar
    image_tag("#{avatar_name}", class: "avatar")
  end

  def full_name
    hande_none user_profile.full_name do
      user_profile.full_name
    end
  end

  def member_since
    user_profile.created_at.strftime("%B %e, %Y")
  end

  def phone
    hande_none user_profile.phone do
      user_profile.phone
    end
  end

  def fax
    hande_none user_profile.fax do
      user_profile.fax
    end
  end

  def info
    hande_none user_profile.info do
      markdown(user_profile.info)
    end
  end

  def address
    hande_none user_profile.address do
      markdown(user_profile.address)
    end
  end

private

  def hande_none(value)
    if value.present?
      yield
    else
      h.content_tag :span, "None given", class: "none"
    end
  end

  def avatar_name
    if user_profile.avatar.present?
      mpub_image_url(id: user_profile.id, type: :thumb, uploader: user_profile.avatar, cname: c_name(user_profile))
    else
      "defaults/avatar.png"
    end
  end
end