class CompanyPresenter < BasePresenter
  presents :company
  delegate :name, to: :company

  def linked_name
    site_link(company.name.present? ? company.name : "Unknown")
  end

  def avatar
    site_link image_tag("#{avatar_name}", class: "avatar")
  end

  def member_since
    company.created_at.strftime("%B %e, %Y")
  end

  def phone
    hande_none company.phone do
      company.phone
    end
  end

  def fax
    hande_none company.fax do
      company.fax
    end
  end

  def website
    hande_none company.website do
      h.link_to(company.website, url_with_protocol(company.website))
    end
  end

  def info
    hande_none company.info do
      markdown(company.info)
    end
  end

  def address
    hande_none company.address do
      markdown(company.address)
    end
  end

private

  def hande_none(value)
    if value.present?
      yield
    else
      h.content_tag :span, "None given", class: "none"
    end
  end

  def site_link(content)
    h.link_to_if(company.website.present?, content, url_with_protocol(company.website))
  end

  def avatar_name
    if company.avatar.present?
      mpub_image_url(id: company.id, type: :thumb, uploader: company.avatar, cname: c_name(company))
    else
      "defaults/avatar.png"
    end
  end
end