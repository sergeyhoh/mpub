# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#sub_category_category_name').typeahead
    source: $('#sub_category_category_name').data('autocomplete-source')

  $('#category_name').typeahead
    source: $('#category_name').data('autocomplete-source')

  # Sorting links.
  $('#sub_categories_list th a').on 'click', () ->
    $.getScript(this.href)
    false
