class ContactsController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:new, :create]
  before_filter :load_companies, :only => [:index, :show]
  before_filter :admin_role, :only => [:index]

  helper_method :sort_column, :sort_direction

  # GET /contacts
  # GET /contacts.json
  def index
    if @is_admin
      @contacts = Contact.order(sort_column + ' ' + sort_direction)
    else
      @contacts = Contact.where(company_id: current_user.company_id).order(sort_column + ' ' + sort_direction)
    end
    authorize! :index, Contact, message: 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # index.html.erb
      format.js
      #format.json { render json: @contacts }
    end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @contact = Contact.find(params[:id])
    authorize! :show, @contact, :message => 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # show.html.erb
      #format.json { render json: @contact }
    end
  end

  # GET /contacts/new
  # GET /contacts/new.json
  def new
    @contact = Contact.new
    #authorize! :new, @contact, :message => 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # new.html.erb
      #format.json { render json: @contact }
    end
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(params[:contact])
    #authorize! :create, @contact, :message => 'You are not authorized to create new record.'

    respond_to do |format|
      if @contact.save
        format.html { redirect_to root_path, notice: 'Contact was successfully created.' }
        #format.json { render json: @contact, status: :created, location: @contact }
      else
        format.html { render action: "new" }
        #format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact = Contact.find(params[:id])
    authorize! :destroy, @contact, :message => 'You are not authorized to delete this record.'
    @contact.destroy

    respond_to do |format|
      format.html { redirect_to contacts_url }
      #format.json { head :no_content }
    end
  end

  private
    def load_companies
      @companies_hash = {}

      Company.select("id, name").order(:name).each do |company|
        @companies_hash[company.id] = company.name
      end
    end

    def sort_column
      Contact.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "desc"
    end

    def admin_role
      @is_admin = current_user.has_role?(:admin)
    end
end
