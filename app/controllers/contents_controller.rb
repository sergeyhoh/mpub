class ContentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_current_user
  before_filter :load_contentable, :except => [:destroy, :download]

  helper_method :contentable_url, :sort_column, :sort_direction

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to contents_url, :alert => 'Invalid content!'
  end

  add_breadcrumb 'Create a new Content', '', :only => [:new, :create]
  add_breadcrumb 'Edit a Content', '', :only => [:edit, :update]

  # GET /contents
  # GET /contents.json
  def index
    authorize! :index, Content, :message => 'You are not authorized to access this page.'

    if @contentable.nil?
      @contents = Content.where(user_id: current_user.id).order(sort_column + ' ' + sort_direction)
    else
      @contents = @contentable.contents.order(sort_column + ' ' + sort_direction)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json # { render json: @contents }
      format.js
    end
  end

  # GET /contents/1
  # GET /contents/1.json
  def show
    if @contentable.nil?
      @content = Content.find(params[:id])
    else
      @content = @contentable.contents.find(params[:id])
    end
    authorize! :show, @content, :message => 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # show.html.erb
      format.json # { render json: @content }
    end
  end

  # GET /contents/new
  # GET /contents/new.json
  def new
    if @contentable.nil?
      @content = Content.new
    else
      @content = @contentable.contents.new

      @content.category_ids = [@contentable.id.to_s] if @contentable.class.name == 'Category'
      @content.sub_category_ids = [@contentable.id.to_s] if @contentable.class.name == 'SubCategory'
    end

    #current_ability.attributes_for(:new, Content).each do |key, value|
    #  @content.send("#{key}=", value)
    #end
    #@content.attributes = params[:content]
    authorize! :new, @content, :message => 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @content }
    end
  end

  # GET /contents/1/edit
  def edit
    if @contentable.nil?
      @content = Content.find(params[:id])
    else
      @content = @contentable.contents.find(params[:id])
    end
    authorize! :edit, @content, :message => 'You are not authorized to access this page.'
  end

  # POST /contents
  # POST /contents.json
  def create
    if @contentable.nil?
      @content = Content.new(params[:content])
    else
      @content = @contentable.contents.new(params[:content])
    end

    #current_ability.attributes_for(:create, Content).each do |key, value|
    #  @content.send("#{key}=", value)
    #end
    #@content.attributes = params[:content]
    authorize! :create, @content, :message => 'You are not authorized to create new record.'

    @content.user_id = current_user.id

    respond_to do |format|
      if @content.save
        format.html { redirect_to contentable_url, notice: 'Content was successfully created.' }
        format.json { render json: @content, status: :created, location: @content }
      else
        format.html { render action: "new" }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /contents/1
  # PUT /contents/1.json
  def update
    if @contentable.nil?
      @content = Content.find(params[:id])
    else
      @content = @contentable.contents.find(params[:id])
    end
    authorize! :update, @content, :message => 'You are not authorized to update this record.'

    respond_to do |format|
      if @content.update_attributes(params[:content])
        format.html { redirect_to contentable_url, notice: 'Content was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contents/1
  # DELETE /contents/1.json
  def destroy
    @content = Content.find(params[:id])
    authorize! :destroy, @content, :message => 'You are not authorized to delete this record.'
    @content.destroy

    respond_to do |format|
      format.html { redirect_to contentable_url }
      format.json { head :no_content }
    end
  end

  def unlink
    if @contentable.nil?
      @content = Content.find(params[:id])
    else
      @content = @contentable.contents.find(params[:id])
    end
    authorize! :unlink, @content, :message => 'You are not authorized to update this record.'

    respond_to do |format|
      if (@content.category_ids.length + @content.sub_category_ids.length) > 1 && !@contentable.nil? && @contentable.contents.delete(@content)
        format.html { redirect_to contentable_url, notice: 'Content link was successfully unlinked.' }
        format.json { head :no_content }
      else
        format.html { redirect_to contentable_url, alert: "Can't unlink content." }
        format.json { head :no_content }
      end
    end
  end

  def download
    @content = Content.find(params[:id])
    authorize! :download, @content, :message => 'You are not authorized to download this record.'

    path = "#{Rails.root}#{@content.file.url}"
    send_file path, :x_sendfile=>true
  end

  def activate
    if @contentable.nil?
      @content = Content.find(params[:id])
    else
      @content = @contentable.contents.find(params[:id])
    end
    authorize! :activate, @content, :message => 'You are not authorized to activate this record.'

    respond_to do |format|
      if @content.update_attribute(:active, !@content.active)
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Content was successfully activated/diactivated.' }
        else
          format.html { redirect_to contentable_url, notice: 'Content was successfully activated/diactivated.' }
          format.js
        end
        format.json { head :no_content }
      else
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), :alert => "Can't activate/diactivate content!" }
        else
          format.html { redirect_to contentable_url, :alert => "Can't activate/diactivate content!" }
          format.js
        end
        format.json { head :no_content }
      end
    end
  end

private

  def load_contentable
    klass = [Category, SubCategory].detect { |c| params["#{c.name.underscore}_id"] }
    if !klass.nil?
      @contentable = klass.where(user_id: current_user.id, id: params["#{klass.name.underscore}_id"]).first
      if klass.name == 'Category'
        add_breadcrumb "#{@contentable.name}", :sub_categories_path, :unless => :is_admin?
      elsif klass.name == 'SubCategory'
        add_breadcrumb "#{@contentable.category.name}", :sub_categories_path, :unless => :is_admin?
        add_breadcrumb "#{@contentable.name}", :sub_category_contents_path, :unless => :is_admin?
      end
    elsif !get_sub_category.blank?
      @contentable = SubCategory.where(user_id: current_user.id, id: get_sub_category).first
      add_breadcrumb "#{@contentable.category.name}", :sub_categories_path, :unless => :is_admin?
      add_breadcrumb "#{@contentable.name}", :contents_path, :unless => :is_admin?
    else
      add_breadcrumb "SubCategories", :sub_categories_path, :unless => :is_admin?
      add_breadcrumb "Contents", :contents_path, :unless => :is_admin?
    end
  end

  def contentable_url
    klass = [Category, SubCategory].detect { |c| params["#{c.name.underscore}_id"] }
    if !klass.nil?
      if klass.name == 'Category'
        sub_categories_path
      elsif klass.name == 'SubCategory'
        sub_category_contents_path
      end
    else
      contents_path
    end
  end

  def get_sub_category
    if params[:sub_category_id]
      session[:sub_category] = params[:sub_category_id]
    else
      session[:sub_category]
    end
  end

  def sort_column
    Content.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
