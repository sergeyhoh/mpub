class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :authenticate_user!

  #check_authorization
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  add_breadcrumb 'Home', :root_path

  private

  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    klass.new(object, view_context)
  end

  def set_current_user
    if current_user.has_role? :admin
      User.current = User.find(session_user_id)
    else
      User.current = current_user
    end
  end

  def session_user_id(user_id = nil)
    user_id.nil? ? session[:user_id] : session[:user_id] = user_id
  end

  def is_admin?
    current_user.has_role?(:admin)
  end
end
