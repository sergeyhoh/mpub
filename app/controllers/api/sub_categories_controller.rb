class Api::SubCategoriesController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:index, :show, :download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid sub-category!" }
  end

  # GET /sub_categories
  # GET /sub_categories.json
  def index
    #authorize! :index, SubCategory, message: 'You are not authorized to access this page.'

    if @read_user
      if params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid
        @sub_categories = SubCategory.where(user_id: @read_user.id).order("category_id ASC, name ASC")
      else
        @sub_categories = SubCategory.where(user_id: @read_user.id, active: true).order("category_id ASC, name ASC")
      end

      #render 'sub_categories/index'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  # GET /sub_categories/1
  # GET /sub_categories/1.json
  def show
    if @read_user
      @sub_category = SubCategory.find(params[:id])

      #render 'sub_categories/show'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  def download
    if @read_user
      @sub_category = SubCategory.find(params[:id])

      path = get_path(@sub_category, params[:type])
      send_file path, :x_sendfile=>true
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end


private

  def get_path(sub_category, type)
    if type
      if type == 'orig'
        "#{Rails.root}#{sub_category.art.url}"
      else
        sub_category.active ? "/#{sub_category.art.url(type.to_sym)}" : "/#{sub_category.art.url(type.to_sym, :sprite)}"
      end
    else
      "/defaults/default_150x100.png"
    end
  end

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end

end
