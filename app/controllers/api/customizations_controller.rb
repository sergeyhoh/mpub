class Api::CustomizationsController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid customization!" }
  end

  def download
    if @read_user
      path = "#{Rails.root}#{@read_user.customization.background_image.url}"
      send_file path, :x_sendfile=>true
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

private

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end

end