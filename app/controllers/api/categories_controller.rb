class Api::CategoriesController < Api::BaseController
  # before_filter :authenticate_user!
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:index, :structure, :show, :download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid category!" }
  end

  # GET /categories
  # GET /categories.json
  def index
    if @read_user
      if params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid
        @categories = Category.where(user_id: @read_user.id).order("name ASC")
      else
        @categories = Category.where(user_id: @read_user.id, active: true).order("name ASC")
      end

      #render 'categories/index'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  # GET /structures
  # GET /structures.json
  def structure
    if @read_user
      if params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid
        @categories = Category.where(user_id: @read_user.id).order("name ASC")
      else
        @categories = Category.where(user_id: @read_user.id, active: true).order("name ASC")
      end

      render 'api/categories/structure'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    if @read_user
      @category = Category.find(params[:id])

      #render 'categories/show'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  def download
    if @read_user
      @category = Category.find(params[:id])

      path = get_path(@category, params[:type])
      send_file path, :x_sendfile=>true
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

private

  def get_path(category, type)
    if type
      if type == 'orig'
        "#{Rails.root}#{category.art.url}"
      else
        category.active ? "/#{category.art.url(type.to_sym)}" : "/#{category.art.url(type.to_sym, :sprite)}"
      end
    else
      "/defaults/default_200x200.png"
    end
  end

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end
end
