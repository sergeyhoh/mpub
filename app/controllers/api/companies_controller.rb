class Api::CompaniesController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:info, :download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid company!" }
  end

  # GET /companies/1
  # GET /companies/1.json
  def info
    if @read_user
      @company = Company.find(@read_user.company_id)

      #render 'categories/show'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  def download
    if @read_user
      @company = Company.find(@read_user.company_id)

      if params[:type] == 'orig'
        path = "#{Rails.root}#{@company.avatar.url}"
        send_file path, :x_sendfile=>true
      elsif params[:type] == 'thumb'
        path = "#{Rails.root}#{@company.avatar.thumb.url}"
        send_file path, :x_sendfile=>true
      end
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

private

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end
end
