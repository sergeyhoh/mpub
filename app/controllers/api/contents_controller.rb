class Api::ContentsController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:index, :show, :download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid content!" }
  end

  # GET /contents
  # GET /contents.json
  def index
    if @read_user
      if params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid
        @contents = Content.where(user_id: @read_user.id).order("file ASC")
      else
        @contents = Content.where(user_id: @read_user.id, active: true).order("file ASC")
      end

      #render 'contents/index'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  # GET /contents/1
  # GET /contents/1.json
  def show
    if @read_user
      @content = Content.find(params[:id])

      #render 'contents/show'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  def download
    if @read_user
      @content = Content.find(params[:id])

      path = "#{Rails.root}#{@content.file.url}"
      send_file path, :x_sendfile=>true
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

private

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end

end
