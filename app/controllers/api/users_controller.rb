class Api::UsersController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:info, :updated]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid user!" }
  end

  def info
    if @read_user
      @user = @read_user

      #render 'users/info'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

  def updated
    if @read_user
      @user = @read_user

      #render 'users/updated'
    else
      render :json=> { :success => false, :message => "Invalid user!" }
    end
  end

private

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end
end
