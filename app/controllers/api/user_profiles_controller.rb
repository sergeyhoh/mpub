class Api::UserProfilesController < Api::BaseController
  before_filter { |c| c.send(:authenticate_by_user_token, params[:user_token]) }
  skip_before_filter :authenticate_user!, :only => [:download]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid user profile!" }
  end

  def download
    if @read_user
      if params[:type] == 'orig'
        path = "#{Rails.root}#{@read_user.user_profile.avatar.url}"
        send_file path, :x_sendfile=>true
      elsif params[:type] == 'thumb'
        path = "#{Rails.root}#{@read_user.user_profile.avatar.thumb.url}"
        send_file path, :x_sendfile=>true
      end
    else
      render :json=> { :success => false, :message => "Invalid user profile!" }
    end
  end

private

  def authenticate_by_user_token(user_token)
    if user_token
      @read_user = User.find_by_user_token(user_token)
    end
  end

end