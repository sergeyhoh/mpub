class Api::SessionsController < Api::BaseController
  before_filter :authenticate_user!, :except => [:create, :destroy, :status]
  before_filter :ensure_login_params_exist, :except => [:status]
  before_filter :ensure_token_params_exist, :except => [:create, :destroy]
  respond_to :json

  def create
    resource = User.find_for_database_authentication(:email => params[:user_login][:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:user_login][:password])
      sign_in(:user, resource)
      resource.ensure_authentication_token!
      render :json=> {:success=>true, :auth_token=>resource.authentication_token, :user_id => resource.id, :email=>resource.email}
      return
    end
    invalid_login_attempt
  end

  def destroy
    resource = User.find_for_database_authentication(:email => params[:user_login][:email])
    resource.authentication_token = nil
    resource.save
    render :json=> {:success=>true}
  end

  def status
    resource = User.find_for_token_authentication(auth_token: params[:auth_token])
    if resource
      render :json=> { :success => true }
    else
      render :json=> { :success => false, :message => "Incorrect auth_token parameter" }
    end
  end

  protected
  def ensure_login_params_exist
    return unless params[:user_login].blank?
    render :json=>{:success=>false, :message=>"Missing user_login parameter"}, :status=>422
  end

  def ensure_token_params_exist
    return unless params[:auth_token].blank?
    render :json=>{:success=>false, :message=>"Missing auth_token parameter"}, :status=>422
  end

  def invalid_login_attempt
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end
end