class Api::ContactsController < Api::BaseController
  skip_before_filter :authenticate_user!, :only => [:create]

  rescue_from ActiveRecord::RecordNotFound do
    render :json=> { :success => false, :message => "Invalid contact!" }
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(params[:contact])

    if @contact.save
      render json: { success: true }
    else
      render json: { success: false, reason: @contact.errors, }
    end
  end

end
