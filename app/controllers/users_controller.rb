class UsersController < ApplicationController
  before_filter :authenticate_user!
  #before_filter :set_current_user, only: [:index, :new, :edit, :create, :update, :show]
  before_filter :get_user, only: [:index, :new, :edit]
  before_filter :get_roles, only: [:new, :edit, :create, :update]
  before_filter :get_companies, only: [:index]

  helper_method :sort_column, :sort_direction

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to users_url, :alert => 'Invalid user!'
  end

  #add_breadcrumb "Users", :users_path, :only => [:index, :show, :new, :create]

  def index
    authorize! :index, User, message: 'Not authorized as an administrator.'
    # if current_user.has_role? :admin
    #   @users = User.paginate(page: params[:page], per_page: 30).order(:name)
    # else
    #   @users = User.where(company_id: current_user.company_id).paginate(page: params[:page], per_page: 30).order(:name)
    # end
    @users = User.accessible_by(current_ability).paginate(page: params[:page], per_page: 30).order(sort_column + ' ' + sort_direction)

    respond_to do |format|
      format.html # show.html.erb
      format.json
      format.js
    end
  end

  def show
    @user = User.find(params[:id])
    authorize! :show, @user, message: 'Not authorized as an administrator.'

    # Set User.current to edited user
    session_user_id(params[:id])

    add_breadcrumb "#{@user.name}"

    @categories = Category.where(user_id: params[:id]).order(:name)
    @sub_categories = SubCategory.where(user_id: params[:id]).order(:name)
    set_categories_and_sub_categories_hash(@categories, @sub_categories)
    @contents = Content.where(user_id: params[:id]).order(:file)

    respond_to do |format|
      format.html # show.html.erb
      format.json
      format.js
    end
  end

  def new
    @user = User.new
    current_ability.attributes_for(:new, User).each do |key, value|
      @user.send("#{key}=", value)
    end
    @user.attributes = params[:user]
    authorize! :new, @user, message: 'Not authorized as an administrator.'
  end

  def create
    @user = User.new(params[:user])
    current_ability.attributes_for(:create, User).each do |key, value|
      @user.send("#{key}=", value)
    end
    @user.attributes = params[:user]
    authorize! :create, @user, message: 'Not authorized as an administrator.'

    # Company admin can create users only for his company
    unless current_user.has_role? :admin
      @user.company_id = current_user.company_id
    end

    if @user.save
      redirect_to @user
    else
      render :action => :new, :status => :unprocessable_entity
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize! :edit, @user, message: 'Not authorized as an administrator.'

    add_breadcrumb "#{@user.name}"
  end

  def update
    @user = User.find(params[:id])
    authorize! :update, @user, message: 'Not authorized as an administrator.'

    add_breadcrumb "#{@user.name}"

    if params[:user][:password].blank?
      [:password, :password_confirmation].collect{ |p| params[:user].delete(p) }
    end

    # If current user not admin check his current password when hi made some changes
    if !current_user.has_role?(:admin)
      @user.company_id = current_user.company_id
      if !current_user.has_role?(:company_admin)
        @user.errors[:base] << "The password you entered is incorrect" unless @user.valid_password?(params[:user][:current_password])
      end
    end

    # if !current_user.has_role?(:admin)
    #   @user.company_id = current_user.company_id
    #   @user.errors[:base] << "The password you entered is incorrect" unless @user.valid_password?(params[:user][:current_password])
    # end

    # Delete params :current_password to prevent error: Can't mass-assign protected attributes: current_password
    params[:user].delete(:current_password)

    # Check and edit user data
    if @user.errors[:base].empty? and @user.update_attributes(params[:user])
      if current_user.has_role?(:admin) || current_user.has_role?(:company_admin)
        redirect_to @user, notice: "Account has been updated."
      else
        redirect_to root_path
      end
    else
      render action: :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize! :destroy, @user, message: 'Not authorized as an administrator.'

    begin
      @user.destroy
      flash[:notice] = "User #{@user.name} deleted"
    rescue Exception => e
      flash[:error] = e.message
    end

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def info
    @user = User.find(params[:id])
    authorize! :info, @user, message: 'Not authorized as an administrator.'

    respond_to do |format|
      format.json
    end
  end

  def updated
    @user = User.find(params[:id])
    authorize! :updated, @user, message: 'Not authorized as an administrator.'

    respond_to do |format|
      format.json
    end
  end

  # Make the current user object available to views
  #----------------------------------------
  def get_user
    @current_user = current_user
  end

private

  def get_roles
    if current_user.has_role? :admin
      @roles = Role.all
    else
      @roles = Role.find(:all, :conditions => ["name != ?", :admin])
    end
  end

  def get_companies
    @companies = {}

    Company.select('id, name').each do |company|
      @companies[company.id] = company.name
    end
  end

  def set_categories_and_sub_categories_hash(categories, sub_categories)
    @categories_hash = {}
    @sub_categories_hash = {}

    categories.each do |category|
      @categories_hash[category.id] = category.name
    end

    sub_categories.each do |sub_category|
      @sub_categories_hash[sub_category.id] = { category: @categories_hash[sub_category.category_id], name: sub_category.name }
    end
  end

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
