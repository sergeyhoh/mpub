class SubCategoriesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_current_user
  before_filter :load_contentable, only: :index
  before_filter :find_categories, except: :destroy

  helper_method :sort_column, :sort_direction

  add_breadcrumb "SubCategories", :sub_categories_path, :only => [:new, :edit, :show], :unless => :is_admin?
  add_breadcrumb 'Create a new SubCategory', :new_sub_category_path, :only => [:new, :create]
  add_breadcrumb 'Edit a SubCategory', :edit_sub_category_path, :only => [:edit, :update]

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to sub_categories_url, :alert => 'Invalid sub-category!'
  end

  # GET /sub_categories
  # GET /sub_categories.json
  def index
    authorize! :index, SubCategory, message: 'You are not authorized to access this page.'
    authorize! :index, Content, message: 'You are not authorized to access this page.'

    if @contentable.nil?
      @sub_categories = SubCategory.where(user_id: current_user.id).order(sort_column + ' ' + sort_direction)
      add_breadcrumb "SubCategories", :sub_categories_path, :unless => :is_admin?

      @contents = []
    else
      @sub_categories = SubCategory.where(user_id: current_user.id, category_id: get_category).order(sort_column + ' ' + sort_direction)
      add_breadcrumb "#{@contentable.name}", :sub_categories_path, :unless => :is_admin?

      @contents = @contentable.contents.order("name ASC")
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json # { render json: @sub_categories + @contents }
      format.js
    end
  end

  # GET /sub_categories/1
  # GET /sub_categories/1.json
  def show
    @sub_category = SubCategory.find(params[:id])
    authorize! :show, @sub_category, message: 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # show.html.erb
      format.json # { render json: @sub_category }
    end
  end

  # GET /sub_categories/new
  # GET /sub_categories/new.json
  def new
    @sub_category = SubCategory.new
    current_ability.attributes_for(:new, SubCategory).each do |key, value|
      @sub_category.send("#{key}=", value)
    end
    @sub_category.attributes = params[:sub_category]
    authorize! :new, @sub_category, message: 'You are not authorized to access this page.'

    @sub_category.category_id = get_category

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sub_category }
    end
  end

  # GET /sub_categories/1/edit
  def edit
    @sub_category = SubCategory.find(params[:id])
    authorize! :edit, @sub_category, message: 'You are not authorized to access this page.'
  end

  # POST /sub_categories
  # POST /sub_categories.json
  def create
    @sub_category = SubCategory.new(params[:sub_category])
    current_ability.attributes_for(:create, SubCategory).each do |key, value|
      @sub_category.send("#{key}=", value)
    end
    @sub_category.attributes = params[:sub_category]
    authorize! :create, @sub_category, message: 'You are not authorized to create new record.'

    @sub_category.user_id = User.current.id #current_user.id

    respond_to do |format|
      if @sub_category.save
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Camera type was successfully created.' }
        else
          format.html { redirect_to sub_categories_url, notice: 'Camera type was successfully created.' }
        end
        format.json { render json: @sub_category, status: :created, location: @sub_category }
      else
        format.html { render action: "new" }
        format.json { render json: @sub_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sub_categories/1
  # PUT /sub_categories/1.json
  def update
    @sub_category = SubCategory.find(params[:id])
    authorize! :update, @sub_category, message: 'You are not authorized to update this record.'

    respond_to do |format|
      if @sub_category.update_attributes(params[:sub_category])
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Camera type was successfully updated.' }
        else
          format.html { redirect_to sub_categories_url, notice: 'Camera type was successfully updated.' }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sub_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_categories/1
  # DELETE /sub_categories/1.json
  def destroy
    @sub_category = SubCategory.find(params[:id])
    authorize! :destroy, @sub_category, message: 'You are not authorized to delete this record.'
    @sub_category.destroy

    respond_to do |format|
      format.html { redirect_to sub_categories_url }
      format.json { head :no_content }
    end
  end

  def download
    @sub_category = SubCategory.find(params[:id])
    authorize! :download, @sub_category, :message => 'You are not authorized to download this record.'

    path = get_path(@sub_category, params[:type])
    send_file path, :x_sendfile=>true
  end

  def activate
    @sub_category = SubCategory.find(params[:id])
    authorize! :activate, @sub_category, :message => 'You are not authorized to activate this record.'

    respond_to do |format|
      if @sub_category.update_attributes(:active => !@sub_category.active)
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'SubCategory was successfully activated/diactivated.' }
        else
          format.html { redirect_to sub_categories_url, notice: 'SubCategory was successfully activated/diactivated.' }
          format.js
        end
        format.json { head :no_content }
      else
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), :alert => "Can't activate/diactivate SubCategory!" }
        else
          format.html { redirect_to sub_categories_url, :alert => "Can't activate/diactivate SubCategory!" }
          format.js
        end
        format.json { head :no_content }
      end
    end
  end

private

  def find_categories
    @categories_hash = {}
    @categories_array = []

    Category.select("id, name").where(user_id: User.current.id).order(:name).each do |category|
      @categories_hash[category.id] = category.name
      @categories_array << category.name
    end
  end

  def load_contentable
    @contentable = get_category.blank? ? nil : Category.find(get_category)
  end

  def get_category
    if params[:category_id]
      session[:category] = params[:category_id]
    else
      session[:category]
    end
  end

  def get_path(sub_category, type)
    if type
      if type == 'orig'
        "#{Rails.root}#{sub_category.art.url}"
      else
        sub_category.active ? "#{Rails.root}#{sub_category.art.url(type.to_sym)}" : "#{Rails.root}#{sub_category.art.url(type.to_sym, :sprite)}"
      end
    else
      "/defaults/default_150x100.png"
    end
  end

  def sort_column
    SubCategory.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

end
