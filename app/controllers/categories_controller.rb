class CategoriesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_current_user

  helper_method :sort_column, :sort_direction

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to categories_url, :alert => 'Invalid category!'
  end

  add_breadcrumb 'Create a new Category', :new_category_path, :only => [:new, :create]
  add_breadcrumb 'Edit a Category', :edit_category_path, :only => [:edit, :update]

  # GET /categories
  # GET /categories.json
  def index
    authorize! :index, Category, message: 'You are not authorized to access this page.'

    @categories = Category.where(user_id: current_user.id).order(sort_column + ' ' + sort_direction)
    set_categories_hash(@categories)

    respond_to do |format|
      format.html # index.html.erb
      format.json # { render json: @categories }
      format.js
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category = Category.find(params[:id])
    authorize! :show, @category, message: 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # show.html.erb
      format.json # { render json: @category }
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @category = Category.new
    current_ability.attributes_for(:new, Category).each do |key, value|
      @category.send("#{key}=", value)
    end
    @category.attributes = params[:category]
    authorize! :new, @category, message: 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
    authorize! :edit, @category, message: 'You are not authorized to access this page.'
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(params[:category])
    current_ability.attributes_for(:create, Category).each do |key, value|
      @category.send("#{key}=", value)
    end
    @category.attributes = params[:category]
    authorize! :create, @category, message: 'You are not authorized to create new record.'

    @category.user_id = User.current.id #current_user.id

    respond_to do |format|
      if @category.save
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Category was successfully created.' }
        else
          format.html { redirect_to categories_url, notice: 'Category was successfully created.' }
        end
        format.json { render json: @category, status: :created, location: @category }
      else
        format.html { render action: "new" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    @category = Category.find(params[:id])
    authorize! :update, @category, message: 'You are not authorized to update this record.'

    respond_to do |format|
      if @category.update_attributes(params[:category])
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Category was successfully updated.' }
        else
          format.html { redirect_to categories_url, notice: 'Category was successfully updated.' }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category = Category.find(params[:id])
    authorize! :destroy, @category, message: 'You are not authorized to delete this record.'
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
      format.json { head :no_content }
    end
  end

  def download
    @category = Category.find(params[:id])
    authorize! :download, @category, :message => 'You are not authorized to download this record.'

    path = get_path(@category, params[:type])
    send_file path, :x_sendfile=>true
  end

  def activate
    @category = Category.find(params[:id])
    authorize! :activate, @category, :message => 'You are not authorized to activate this record.'

    respond_to do |format|
      if @category.update_attributes(:active => !@category.active)
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), notice: 'Category was successfully activated/diactivated.' }
        else
          format.html { redirect_to categories_url, notice: 'Category was successfully activated/diactivated.' }
          format.js
        end
        format.json { head :no_content }
      else
        if current_user.has_role? :admin
          format.html { redirect_to user_url(:id => User.current.id), alert: "Can't activate/diactivate Category!" }
        else
          format.html { redirect_to categories_url, :alert => "Can't activate/diactivate Category!" }
          format.js
        end
        format.json { head :no_content }
      end
    end
  end

private

  def set_categories_hash(categories)
    @chash = {}
    @categories.each do |cat|
      @chash[cat.id] = cat
    end
  end

  def get_path(category, type)
    if type
      if type == 'orig'
        "#{Rails.root}#{category.art.url}"
      else
        category.active ? "#{Rails.root}#{category.art.url(type.to_sym)}" : "#{Rails.root}#{category.art.url(type.to_sym, :sprite)}"
      end
    else
      "/defaults/default_200x200.png"
    end
  end

  def sort_column
    Category.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

end
