class UserProfilesController < ApplicationController
  before_filter :authenticate_user!

  add_breadcrumb "Profile"

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to users_url, :alert => 'Invalid user profile!'
  end

  # GET /user_profiles/1
  # GET /user_profiles/1.json
  def show
    @user_profile = UserProfile.find(params[:id])
    authorize! :show, @user_profile, :message => 'Not authorized as an administrator.'

    respond_to do |format|
      format.html # show.html.erb
      format.json # { render json: @user_profile }
    end
  end

  def download
    @user_profile = UserProfile.find(params[:id])
    authorize! :download, @user_profile, :message => 'You are not authorized to download this record.'

    if params[:type] == 'orig'
      path = "#{Rails.root}#{@user_profile.avatar.url}"
      send_file path, :x_sendfile=>true
    elsif params[:type] == 'thumb'
      path = "#{Rails.root}#{@user_profile.avatar.thumb.url}"
      send_file path, :x_sendfile=>true
    end
  end
end
