class HomeController < ApplicationController
  before_filter :authenticate_user!

  add_breadcrumb "Home", :root_path

  def index
  end
end
