class CompaniesController < ApplicationController
  before_filter :authenticate_user!

  rescue_from ActiveRecord::RecordNotFound do
    redirect_to companies_url, :alert => 'Invalid company!'
  end

  add_breadcrumb 'Create a new Company', :new_company_path, :only => [:new, :create]
  add_breadcrumb 'Edit a Company', :edit_company_path, :only => [:edit, :update]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
    authorize! :index, Company, message: 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @companies }
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @company = Company.find(params[:id])
    authorize! :show, @company, :message => 'You are not authorized to access this page.'

    add_breadcrumb "#{@company.name}"

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    @company = Company.new
    current_ability.attributes_for(:new, Company).each do |key, value|
      @company.send("#{key}=", value)
    end
    @company.attributes = params[:company]
    authorize! :new, @company, :message => 'You are not authorized to access this page.'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/1/edit
  def edit
    @company = Company.find(params[:id])
    authorize! :edit, @company, :message => 'You are not authorized to access this page.'
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(params[:company])
    current_ability.attributes_for(:create, Company).each do |key, value|
      @company.send("#{key}=", value)
    end
    @company.attributes = params[:company]
    authorize! :create, @company, :message => 'You are not authorized to create new record.'

    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render json: @company, status: :created, location: @company }
      else
        format.html { render action: "new" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = Company.find(params[:id])
    authorize! :update, @company, :message => 'You are not authorized to update this record.'

    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = Company.find(params[:id])
    authorize! :destroy, @company, :message => 'You are not authorized to delete this record.'
    @company.destroy

    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end

  def download
    @company = Company.find(params[:id])
    authorize! :download, @company, :message => 'You are not authorized to download this record.'

    if params[:type] == 'orig'
      path = "#{Rails.root}#{@company.avatar.url}"
      send_file path, :x_sendfile=>true
    elsif params[:type] == 'thumb'
      path = "#{Rails.root}#{@company.avatar.thumb.url}"
      send_file path, :x_sendfile=>true
    end
  end
end
