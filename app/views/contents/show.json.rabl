object @content
#cache @content

attributes :id, :user_id, :active, :name, :extension, :file_size, :content_type, :md5_sum, :created_at
node(:file_url) { |content| mpub_file_url(id: content.id, uploader: content.file, cname: c_name(content)) }

node(:updated_at) { |content| content.updated_at.to_i }
