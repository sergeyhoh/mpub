collection @categories
#cache @categories

#extends "categories/show"

attributes :id, :user_id, :active, :name

node(:icon_url) { |cat| cat.art.blank? ? nil : mpub_image_url(id: cat.id, type: :thumb, uploader: cat.art, cname: c_name(cat)) }

node(:updated_at) { |cat| cat.updated_at.to_i }
