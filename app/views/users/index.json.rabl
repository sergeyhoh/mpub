collection @users
#cache @users

attributes :id, :name, :email

if can? :read, Role
  node(:role) { |user| user.roles.first.name.humanize }
end

node(:updated_at) { |user| user.updated_at.to_i }
