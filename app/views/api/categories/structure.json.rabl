object false
node(:success) { :true }

node :categories do
  partial('api/categories/mpub_structure.json', :object => @categories)
end
