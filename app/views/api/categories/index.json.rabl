object false
node(:success) { :true }

node :categories do
  partial('api/categories/categories.json', :object => @categories)
end
