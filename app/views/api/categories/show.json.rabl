object false
node(:success) { :true }

node :category do
  partial('api/categories/category.json', :object => @category)
end
