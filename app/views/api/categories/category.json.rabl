object @category
#cache @category

if root_object.active || (params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid)
  attributes :id, :user_id, :active, :name

  node :icon_urls do |cat|
    if cat.art.blank?
      nil
    else
      Hash[
        [:orig, :thumb, :small].map { |t|
          [t, mpub_api_image_url(id: cat.id, type: t, uploader: cat.art, cname: c_name(cat))]
        }
      ]
    end
  end

  node(:updated_at) { |cat| cat.updated_at.to_i }

  child :sub_categories do
    # attributes :id, :user_id, :active, :name
    # node :icon_urls do |sub_cat|
    #   if sub_cat.art.blank?
    #     nil
    #   else
    #     Hash[
    #       [:orig, :thumb, :small].map { |t|
    #         [t, mpub_api_image_url(id: sub_cat.id, type: t, uploader: sub_cat.art, cname: c_name(sub_cat))]
    #       }
    #     ]
    #   end
    # end

    # child :contents do
    #   attributes :id, :user_id, :active, :extension, :file_size, :content_type, :md5_sum
    #   node(:file_url) { |content| mpub_api_file_url(id: content.id, uploader: content.file, cname: c_name(content)) }
    # end

    extends "api/sub_categories/sub_category"
  end

  child :contents do
    # attributes :id, :user_id, :active, :extension, :file_size, :content_type, :md5_sum
    # node(:file_url) { |content| mpub_api_file_url(id: content.id, uploader: content.file, cname: c_name(content)) }

    # node(:updated_at) { |content| content.updated_at.to_i }

    extends "api/contents/content"
  end

end
