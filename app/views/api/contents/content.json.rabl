object @content
#cache @content

if root_object.active || (params[:device_udid] && @read_user.device_udid && params[:device_udid] == @read_user.device_udid)
  attributes :id, :user_id, :active, :name, :extension, :file_size, :content_type, :md5_sum, :created_at
  node(:file_url) { |content| mpub_api_file_url(id: content.id, uploader: content.file, cname: c_name(content)) }

  node(:updated_at) { |content| content.updated_at.to_i }
end