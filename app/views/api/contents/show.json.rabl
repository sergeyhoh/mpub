object false
node(:success) { :true }

node :content do
  partial('api/contents/content.json', :object => @content)
end
