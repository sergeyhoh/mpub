object @company
#cache @company

attributes :id, :name, :info, :address, :fax, :phone, :website

node :avatar_urls do |company|
  if company.avatar.blank?
    nil
  else
    Hash[
      [:orig, :thumb].map { |t|
        [t, mpub_api_image_url(id: company.id, type: t, uploader: company.avatar, cname: c_name(company))]
      }
    ]
  end
end

node(:updated_at) { |company| company.updated_at.to_i }
