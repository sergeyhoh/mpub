object false
node(:success) { :true }

node :company do
  partial('api/companies/company.json.rabl', :object => @company)
end

