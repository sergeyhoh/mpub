object @user
#cache @user

attributes :id, :name, :email

node(:updated_at) { |user| user.updated_at.to_i }
#node(:data_updated_at) { |user| user.api_updated_at.to_i }

child :user_profile do
  attributes :id, :user_id, :full_name, :info, :address, :fax, :phone

  node :avatar_urls do |user_profile|
    if user_profile.avatar.blank?
      nil
    else
      Hash[
        [:orig, :thumb].map { |t|
          [t, mpub_api_image_url(id: user_profile.id, type: t, uploader: user_profile.avatar, cname: c_name(user_profile))]
        }
      ]
    end
  end
end

child :customization do
  attributes :id, :user_id, :background_color , :text_color

  node :background_image_urls do |customization|
    if customization.background_image.blank?
      nil
    else
      Hash[
        [:orig].map { |t|
          [t, mpub_api_image_url(id: customization.id, type: t, uploader: customization.background_image, cname: c_name(customization))]
        }
      ]
    end
  end
end
