object false
node(:success) { :true }

node :user do
  partial('api/users/user.json.rabl', :object => @user)
end
