object false
node(:success) { :true }

node :sub_categories do
  partial('api/sub_categories/sub_categories.json', :object => @sub_categories)
end
