collection @sub_categories
#cache @sub_categories

#extends "sub_categories/show"

attributes :id, :user_id, :active, :name

node(:icon_url) { |sub_cat| sub_cat.art.blank? ? nil : mpub_api_image_url(id: sub_cat.id, type: :thumb, uploader: sub_cat.art, cname: c_name(sub_cat)) }

node(:updated_at) { |sub_cat| sub_cat.updated_at.to_i }