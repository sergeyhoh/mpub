object @user_profile
#cache @user_profile

attributes :id, :user_id, :full_name, :info, :address, :fax, :phone

node :avatar_urls do |user_profile|
  if user_profile.avatar.blank?
    nil
  else
    Hash[
      [:orig, :thumb].map { |t|
        [t, mpub_image_url(id: user_profile.id, type: t, uploader: user_profile.avatar, cname: c_name(user_profile))]
      }
    ]
  end
end

node(:updated_at) { |user_profile| user_profile.updated_at.to_i }
