object @sub_category
#cache @sub_category

attributes :id, :user_id, :active, :name

node :icon_urls do |sub_cat|
  if sub_cat.art.blank?
    nil
  else
    Hash[
      [:orig, :thumb, :small].map { |t|
        [t, mpub_image_url(id: sub_cat.id, type: t, uploader: sub_cat.art, cname: c_name(sub_cat))]
      }
    ]
  end
end

node(:updated_at) { |sub_cat| sub_cat.updated_at.to_i }

child :contents do
  attributes :id, :user_id, :active, :extension, :file_size, :content_type, :md5_sum
  node(:file_url) { |content| mpub_file_url(id: content.id, uploader: content.file, cname: c_name(content)) }
end