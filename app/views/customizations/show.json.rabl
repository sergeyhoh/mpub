object @customization
#cache @customization

attributes :id, :user_id, :background_color , :text_color

node :background_image_urls do |customization|
  if customization.background_image.blank?
    nil
  else
    Hash[
      [:orig].map { |t|
        [t, mpub_image_url(id: customization.id, type: t, uploader: customization.background_image, cname: c_name(customization))]
      }
    ]
  end
end

node(:updated_at) { |customization| customization.updated_at.to_i }
