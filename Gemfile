source 'https://rubygems.org'

gem 'rails', '3.2.11'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'sqlite3', :group => [:development, :test]
gem 'mysql2', :group => [:production]

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'

  gem 'bootstrap-sass', '>= 2.0.1'
  gem 'jquery-minicolors-rails'
  gem 'chosen-rails'
end

gem 'jquery-rails'

gem 'will_paginate', '~> 3.0'

gem 'rspec-rails', '>= 2.12.0', :group => [:development, :test]
gem 'factory_girl_rails', '>= 3.1.0', :group => [:development, :test]
#gem 'email_spec', '>= 1.2.1', :group => :test
#gem 'cucumber-rails', '>= 1.3.0', :group => :test
#gem 'capybara', '>= 1.1.2', :group => :test
#gem 'database_cleaner', '>= 0.7.2', :group => :test
gem 'launchy', '>= 2.1.0', :group => :test
gem 'devise', '>= 2.1.2'
gem 'cancan', '>= 1.6.8'
gem 'rolify', '>= 3.2.0'

# Ruby API Builder Language
gem 'rabl'
# JSON parser
gem 'oj'

group :test do
  gem 'cucumber-rails', '>= 1.3.0'
  gem 'capybara', '>= 1.1.2'
  gem 'database_cleaner', '>= 0.7.2'
  gem 'email_spec', '>= 1.2.1'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
gem 'unicorn'

# Deploy with Capistrano
group :development do
  gem 'capistrano'
  gem 'rvm-capistrano'
end

# To use debugger
# gem 'debugger'

# Library for Markdown processing
gem 'redcarpet'

# Provides a simple and extremely flexible way to upload files from Ruby applications.
gem 'rmagick'
gem "mini_magick"
gem 'carrierwave'
gem 'mime-types'
gem 'ckeditor', :git => 'git://github.com/sergeyhoh/ckeditor.git'

gem 'breadcrumbs_on_rails'
