class RoleConstraint
  def initialize(*roles)
    @roles = roles
  end

  def matches?(request)
    user_role = request.env['warden'].user.try(:roles)
    if user_role
      @roles.include? request.env['warden'].user.try(:roles)[0].name.to_sym
    else
      false
    end
  end
end