class ChangeActiveDefalutForCategories < ActiveRecord::Migration
  def change
    change_column :categories, :active, :boolean, :default => false
  end
end
