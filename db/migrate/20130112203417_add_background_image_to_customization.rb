class AddBackgroundImageToCustomization < ActiveRecord::Migration
  def change
    add_column :customizations, :background_image, :string
  end
end
