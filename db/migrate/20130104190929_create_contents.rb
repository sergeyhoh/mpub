class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.integer :sub_category_id
      t.integer :user_id
      t.string :file
      t.string :extension
      t.integer :file_size
      t.string :content_type
      t.string :md5_sum

      t.timestamps
    end
  end
end
