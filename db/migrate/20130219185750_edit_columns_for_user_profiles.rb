class EditColumnsForUserProfiles < ActiveRecord::Migration
  def change
    remove_column :user_profiles, :website
    remove_column :user_profiles, :company
    rename_column :user_profiles, :company_info, :info
  end
end
