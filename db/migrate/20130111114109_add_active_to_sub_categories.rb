class AddActiveToSubCategories < ActiveRecord::Migration
  def change
    add_column :sub_categories, :active, :boolean, :default => true
  end
end
