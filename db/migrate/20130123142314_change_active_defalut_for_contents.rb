class ChangeActiveDefalutForContents < ActiveRecord::Migration
  def change
    change_column :contents, :active, :boolean, :default => false
  end
end
