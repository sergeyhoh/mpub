class CreateContentions < ActiveRecord::Migration
  def change
    create_table :contentions do |t|
      t.integer :content_id
      t.belongs_to :contentable, polymorphic: true

      t.timestamps
    end
    add_index :contentions, [:contentable_id, :contentable_type]
    add_index :contentions, :content_id
  end
end
