class AddUserIdToSubCategory < ActiveRecord::Migration
  def change
    add_column :sub_categories, :user_id, :integer
  end
end
