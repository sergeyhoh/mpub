class AddApiUpdatedAtToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :api_updated_at, :datetime

    User.all.each do |user|
      user.api_updated_at = user.created_at
      user.save!
    end

    change_column :users, :api_updated_at, :datetime, :null => false
  end

  def self.down
    remove_column :users, :api_updated_at
  end
end
