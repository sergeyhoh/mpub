class AddDeviceUdidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :device_udid, :string
  end
end
