class CreateCustomizations < ActiveRecord::Migration
  def change
    create_table :customizations do |t|
      t.integer :user_id
      t.string :background, :default => '#FFFFFF'
      t.string :text_color, :default => '#333333'

      t.timestamps
    end
  end
end
