class RenameBackgroundByHand < ActiveRecord::Migration
  def change
    rename_column :customizations, :background, :background_color
  end
end
