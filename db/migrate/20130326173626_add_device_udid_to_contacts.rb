class AddDeviceUdidToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :device_udid, :string
  end
end
