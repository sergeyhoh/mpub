class RemoveColumnFromCategories < ActiveRecord::Migration
  def change
    remove_column :contents, :sub_category_id
  end
end
