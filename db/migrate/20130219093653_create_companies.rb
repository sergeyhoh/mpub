class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :avatar
      t.string :name
      t.string :phone
      t.string :fax
      t.string :website
      t.text :info
      t.text :address

      t.timestamps
    end
  end
end
