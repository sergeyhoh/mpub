class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.integer :user_id
      t.string :avatar
      t.string :full_name
      t.string :phone
      t.string :fax
      t.string :website
      t.string :company
      t.text :company_info
      t.text :address

      t.timestamps
    end
  end
end
