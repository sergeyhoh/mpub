class AddAditionalFieldsToSubCategories < ActiveRecord::Migration
  def change
    add_column :sub_categories, :art, :string
    add_column :sub_categories, :description, :text
  end
end
