class ChangeActiveDefalutForSubCategories < ActiveRecord::Migration
  def change
    change_column :sub_categories, :active, :boolean, :default => false
  end
end
