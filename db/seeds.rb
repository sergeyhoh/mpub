# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'SETTING UP DEFAULT USER LOGIN'

company = Company.create :name => 'First Company'

user = User.create! :name => 'First User', :email => 'user@example.com', :password => 'please', :password_confirmation => 'please', :user_profile_attributes => { :full_name => "First User"}
user.add_role :admin
puts 'New user created: ' << user.name

user2 = User.create! :company_id => company.id, :name => 'Second User', :email => 'user2@example.com', :password => 'please', :password_confirmation => 'please', :user_profile_attributes => { :full_name => "Second User"}
user2.add_role :company_admin
puts 'New user created: ' << user2.name

user3 = User.create! :company_id => company.id, :name => 'Third User', :email => 'user3@example.com', :password => 'please', :password_confirmation => 'please', :user_profile_attributes => { :full_name => "Third User"}
user3.add_role :company_user
puts 'New user created: ' << user3.name
